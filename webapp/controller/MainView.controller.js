/* global oComponent_ManagerGR  : true */
sap.ui.define(["ZMM_MNG_GR/controller/BaseController",'sap/m/MessageToast',"sap/ui/model/FilterOperator",
"sap/ui/model/Filter"], function (Controller, MessageToast, FilterOperator,Filter) {
    "use strict";

    return Controller.extend("ZMM_MNG_GR.controller.MainView", {
        onConfirm: function(oEvent){
            debugger;
            var row = oEvent.getParameter("selectedItem").getBindingContext("ODATA").getObject();
            oComponent_ManagerGR.getModel("JSON").setProperty("/RB1" , '');
            oComponent_ManagerGR.getModel("JSON").setProperty("/RB1" , row.Ordernumber);

        },
        onValueHelpSearch: function (oEvent) {
            debugger;
            var sValue = oEvent.getParameter("value");
            var oFilter = [];
            oFilter.push(new Filter("Ordernumber", FilterOperator.Contains, sValue));  
            var oBinding = oEvent.getParameter("itemsBinding");
            oBinding.filter(oFilter);
        },
        onOpenDialog: function(oEvent  , sDialogName){
            if (!this[sDialogName]) {
                this[sDialogName] = sap.ui.xmlfragment("ZMM_MNG_GR.view.fragments." + sDialogName +'Dialog', this);
                this.getView().addDependent(this[sDialogName]);
            }
            this[sDialogName].open();
        },
        onCloseDialog: function (sDialogName) {
            this[sDialogName].close();
        },
        navToGui: function(oEvent){
            var oModel =oComponent_ManagerGR.getModel("JSON").getData();          
           if(oModel.RB1Selected && !!oModel.RB1.trim() ){
            var oParams = {
                PurchaseOrder: oModel.RB1
            };
                this.GoToMigoGr('',oParams);
            }
            else if(oModel.RB2Selected && !!oModel.RB2.trim()){
                var oParams = {
                    xblnr: oModel.RB2
                };
                this.GoToZmmGr('',oParams);
            }
            else{
                MessageToast.show(oComponent_ManagerGR.i18n('ErrorMsg'),{
                    duration: 1500});
                    return;
            }
        },
        GoToZmmGr: function (event , oParams) {
            debugger;
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "ZMM_GR-display";
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                .done((aResponses) => {
                if (aResponses[semamticActionObj].supported === true) {
                var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                target: {
                semanticObject: "ZMM_GR",
                action: "display"
                },
                params: oParams
                })) || "";
                oCrossAppNavigator.toExternal({
                target: {
                shellHash: hash
                }
                });
                }
                
                })
                .fail(function () {});
            },
            GoToMigoGr: function (event , oParams) {
                debugger;
                    var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                    var semamticActionObj = "MIGO_GR-postGoodsReceipt";
                    oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                    if (aResponses[semamticActionObj].supported === true) {
                    var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                    target: {
                    semanticObject: "MIGO_GR",
                    action: "postGoodsReceipt"
                    },
                    params: oParams
                    })) || "";
                    oCrossAppNavigator.toExternal({
                    target: {
                    shellHash: hash
                    }
                    });
                    }
                    
                    })
                    .fail(function () {});
                }
                
    });
});

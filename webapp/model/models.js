sap.ui.define(["sap/ui/model/json/JSONModel", "sap/ui/Device"], function (JSONModel, Device) {
    "use strict";

    return {
        createDeviceModel: function () {
            var oModel = new JSONModel(Device);
            oModel.setDefaultBindingMode("OneWay");
            return oModel;
        },
        createJSONModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
                RB1Selected: false,
                RB2Selected: false,
                RB1: '',
                RB2: ''
			});
			oModel.setSizeLimit(1000);
			return oModel;
		}
    };
});

sap.ui.define(["sap/ui/test/Opa5"], function (Opa5) {
    "use strict";

    return Opa5.extend("ZMM_MNG_GR.test.integration.arrangements.Startup", {
        iStartMyApp: function () {
            this.iStartMyUIComponent({
                componentConfig: {
                    name: "ZMM_MNG_GR",
                    async: true,
                    manifest: true
                }
            });
        }
    });
});

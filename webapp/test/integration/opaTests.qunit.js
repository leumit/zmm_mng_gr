/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require(["/ZMM_MNG_GR/test/integration/AllJourneys"], function () {
        QUnit.start();
    });
});

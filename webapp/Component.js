/* global oComponent_ManagerGR  : true */
var oComponent_ManagerGR;
sap.ui.define(
    ["sap/ui/core/UIComponent", "sap/ui/Device", "ZMM_MNG_GR/model/models"],
    function (UIComponent, Device, models) {
        "use strict";

        return UIComponent.extend("ZMM_MNG_GR.Component", {
            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
             * @public
             * @override
             */
            init: function () {
                oComponent_ManagerGR = this;
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);
                sap.ui.getCore().getConfiguration().setRTL(true);
                //Set HE language
                sap.ui.getCore().getConfiguration().setLanguage("iw_IL");
                this.setModel(models.createJSONModel(), "JSON");
                // enable routing
                this.getRouter().initialize();

                // set the device model
                this.setModel(models.createDeviceModel(), "device");
            },
            i18n: function (str) {
                return oComponent_ManagerGR.getModel("i18n").getProperty(str);
            }
        });
    }
);
